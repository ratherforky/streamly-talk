# css-streamly

## Hoogle

```bash
stack haddock
stack hoogle generate -- --local
stack hoogle server -- --local --port 8000
```

In browser: use custom 'hl' keyword search
  - http://kb.mozillazine.org/Using_keyword_searches