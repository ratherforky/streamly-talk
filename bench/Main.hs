module Main where

import Lib
import Flow


import Criterion.Main
import Streamly.Prelude
import Streamly.Data.Fold

main :: IO ()
main = defaultMain
  [ bgroup "foo"
      [ bench "1" $ nfIO (foldCapital countAs)
      , bench "2" $ nfIO (foldCapital streamlyCount)
      , bench "3" $ nfIO (foldCapital streamlyCount3)
      , bench "chunkedSerial" $ nfIO (parallelCount fromSerial)
      , bench "chunkedConcurrent" $ nfIO (parallelCount fromAhead)

      ]
  ]

-- main = readFile "Capital.txt" >>= (print . foldListCount)
-- main = readFile "Capital.txt" >>= (print . recursiveListCount)
-- main = (streamCharsFromFile "Capital.txt" |> fold countAs) >>= print
foldCapital :: Fold IO Char a -> IO a
foldCapital = foldFile "Capital.txt"

foldFile :: FilePath -> Fold IO Char a -> IO a
foldFile path foldSpec
  =  streamCharsFromFile path
  |> fold foldSpec
