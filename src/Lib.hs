{-# language TypeApplications #-}
module Lib where

import Util

import Flow

import System.IO

import Streamly.Prelude
-- import Streamly.Internal.Data.Stream.Serial
import qualified Streamly.Prelude as Stream
import qualified Streamly.FileSystem.Handle as Handle
-- import qualified Streamly.Unicode.Stream as Stre
import Streamly.Data.Fold (Fold)
import qualified Streamly.Data.Fold as Fold
import Streamly.Internal.Unicode.Stream (decodeLatin1)
import Data.Word
import Control.Monad.IO.Class
import qualified Data.List as List
import Streamly.Data.Fold.Tee
import Streamly.Data.Array.Foreign ( Array )
import qualified Streamly.Data.Array.Foreign as Array
import Streamly.Unicode.Stream (decodeLatin1)
import GHC.Conc (numCapabilities)


libMain :: IO ()
-- main = readFile "Capital.txt" >>= (print . foldListCount)
-- main = readFile "Capital.txt" >>= (print . recursiveListCount)
-- main = (streamCharsFromFile "Capital.txt" |> fold countAs) >>= print
libMain = (streamCharsFromFile "Capital.txt" |> fold streamlyCount3) >>= print

recursiveListCount :: [Char] -> (Int, Int)
recursiveListCount []     = (0,0)
recursiveListCount (x:xs) = case x of
  'a' -> (numAs + 1, numBs    )
  'b' -> (numAs    , numBs + 1)
  _   -> (numAs    , numBs    )
  where
    (numAs, numBs) = recursiveListCount xs

foldListCount :: [Char] -> (Int, Int)
foldListCount = List.foldl' f (0,0)
  where
    f (numAs, numBs) 'a' = (numAs + 1, numBs    )
    f (numAs, numBs) 'b' = (numAs    , numBs + 1)
    f totals _   = totals

streamlyCount :: Monad m => Fold m Char (Int, Int)
streamlyCount = Fold.tee (countThisChar 'a') (countThisChar 'b')

streamlyCount3 :: Monad m => Fold m Char (Int, Int, Int)
streamlyCount3
  = toFold
  $ (,,) <$> Tee (countThisChar 'a')
         <*> Tee (countThisChar 'b')
         <*> Tee (countThisChar 'c')

streamMain :: IO ()
streamMain
  =  (streamCharsFromFile "Capital.txt"
  |> fold countAs)
  >>= print
-- countChars :: FilePath -> IO Int
-- countChars path = do
--   handle <- openFile path ReadMode

-- >>> parallelCountAs fromSerial
-- 93136

-- >>> (streamCharsFromFile "Capital.txt" |> fold countAs)
-- 93136

-- >>> parallelCountAs

-- parallelCount :: IO [Int]
parallelCount streamType
  =  streamChunksFromFile "bigfile.txt" -- Ahead (Array Word8)
  |> Stream.mapM countArray  -- Ahead Int
  |> Stream.maxThreads numCapabilities
  |> streamType
  --  |> fromAhead
  |> Stream.foldl' f (Prelude.replicate 26 0)
  where
    countArray :: Array Word8 -> IO [Int]
    countArray arr
      =  unfold Array.read arr
      |> decodeLatin1
      |> fold countLowercase

    f :: [Int] -> [Int] -> [Int]
    f = Prelude.zipWith (+)

-- streamCharsFromFile :: FilePath -> Serial Char
streamCharsFromFile :: (IsStream t, MonadIO m, MonadIO (t m)) => FilePath -> t m Char
streamCharsFromFile path = do
  handle <- liftIO $ openFile path ReadMode
  unfold Handle.read handle
    |> decodeLatin1
-- streamCharsFromFile path
--   =   liftIO (openFile path ReadMode) -- Serial Handle
--   >>= unfold Handle.read              -- Serial Word8
--    .> decodeLatin1                    -- Serial Char

streamChunksFromFile :: (IsStream t, MonadIO m, MonadIO (t m))
                     => FilePath
                     -> t m (Array Word8)
streamChunksFromFile path = do
  handle <- liftIO $ openFile path ReadMode
  unfold Handle.readChunks handle


-- >>> numberOfAs "lorem-ipsum.txt"
-- 1586
numberOfAs :: FilePath -> IO Int
numberOfAs
  =  streamCharsFromFile
  -- .> fromSerial @SerialT @IO
  .> fold countAs
  -- .> todo

countAs :: Monad m => Fold m Char Int 
countAs = countThisChar 'a'

countThisChar :: Monad m => Char -> Fold m Char Int
countThisChar c = Fold.foldl' f 0
  where
    f :: Int -> Char -> Int
    f total matchedChar
      | matchedChar == c = total + 1
      | otherwise        = total

-- >>> streamCharsFromFile "Capital.txt" |> fold countLowercase
-- [93136,17593,41370,39911,146856,34132,17193,62097,85738,606,5257,49674,31481,82745,100634,27152,1957,76680,77269,111728,38938,12799,17970,3179,20515,215]
countLowercase :: Monad m => Fold m Char [Int]
countLowercase
  =  Fold.distribute [ countThisChar c | c <- alphabet]
  --  |> fmap (zip alphabet)
  where
    alphabet = ['a'..'z'] 

-- >>> countCharacters "short.txt"
-- 44
countCharacters :: FilePath -> IO Int
countCharacters path
  =  streamCharsFromFile path
  |> fold lengthFold

lengthFold :: Monad m => Fold m a Int
lengthFold = Fold.foldr (\_ acc -> acc + 1) 0

streamLength :: Char -> Int -> Int
streamLength _ acc = acc + 1 
-- streamLength _ = (+ 1)

loremHandle :: IO Handle
loremHandle = openFile "lorem-ipsum.txt" ReadMode
