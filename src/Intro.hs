{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TypeApplications #-}
module Intro where

import Control.Concurrent (threadDelay)
import System.IO (stdout)

import Flow

import qualified Streamly.Data.Array.Foreign as Array
import qualified Streamly.Data.Fold as Fold
import qualified Streamly.Data.Unfold as Unfold
import qualified Streamly.Prelude as Stream
import qualified Streamly.FileSystem.Handle as Handle
import qualified Streamly.Unicode.Stream as Unicode
import Data.Functor.Identity (Identity (runIdentity))
import Streamly.Prelude (IsStream (consM), ParallelT, AsyncT, SerialT, parallel, Serial, Parallel, Rate (Rate))
import Streamly.Data.Fold (Fold)
import GHC.Conc (numCapabilities)
import Control.Exception (evaluate)
import Control.Monad ((>=>))
import Control.Monad.IO.Class (MonadIO(liftIO))

-------------------------------------------------------------------------------
-- Network/Concurrency
-------------------------------------------------------------------------------

-- Simulate network/db query by adding a delay
fetch :: String -> IO (String, String)
fetch w = threadDelay 1000000 >> return (w,w)

wordList :: [String]
wordList = ["cat", "dog", "mouse"]

meanings :: [IO (String, String)]
meanings = map fetch wordList

-- | Fetch word meanings for words in 'wordList'. All searches are performed
-- concurrently.
--
getWords :: IO ()
getWords =
      Stream.fromListM meanings                -- AheadT  IO (String, String)
   |> Stream.fromAhead                         -- SerialT IO (String, String)
   |> Stream.map show                          -- SerialT IO String
   |> unlinesBy "\n"                           -- SerialT IO String
   |> Stream.map Array.fromList                -- SerialT IO (Array Word8)
   |> Stream.fold (Handle.writeChunks stdout)  -- IO ()

    where unlinesBy = Stream.intercalateSuffix (Unfold.function id)

-------------------------------------------------------------------------------
-- Numbers
-------------------------------------------------------------------------------

fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib !n = fib (n-1) + fib (n-2)

zeroTo :: IsStream t => Int -> t IO Int
zeroTo = Stream.enumerateFromTo 0

nToZero :: IsStream t => Int -> t IO Int
nToZero !n = Stream.enumerateFromThenTo n (n-1) 0

sumFibsFold :: Fold IO Int Int
sumFibsFold = Fold.sum |> Fold.lmapM (fib .> evaluate)

sumFibs :: Int -> IO Int
sumFibs n
  = nToZero n
--  |> Stream.mapM (Stream.fold sumFibsFold)
--  |> Stream.maxThreads numCapabilities
--  |> Stream.fromParallel
 |> Stream.fold sumFibsFold

test
  = Stream.fromList [30,25] `Stream.parallel` Stream.fromList [35,5,20] --  @AsyncT 5 <> nToZero @AsyncT 6 <> nToZero @AsyncT 2
 |> Stream.mapM (sumFibs >=> evaluate)
 |> Stream.maxThreads numCapabilities  -- AheadT IO (Bool, Counts)
 |> Stream.fromParallel
 |> Stream.toList

delay :: Int -> IO Int
delay n = do
  threadDelay (n * 1000000)   -- sleep for n seconds
  putStrLn (show n ++ " sec") -- print "n sec"
  return n                    -- IO Int

stream1, stream2, stream3, stream4 :: SerialT IO Int
stream1 = Stream.fromListM [delay 3, delay 4]
stream2 = Stream.fromListM [delay 2, delay 1] 
stream3 = Stream.fromList [8, 10] -- Pure values always same order
stream4 = Stream.fromListM [evaluate (fib 15), evaluate $ fib 32] -- IO values can be done in parallel
-- IO return detirmines order. If IO returns lazy result quickly, but the result takes a long time
-- to evaluate (eg `pure (fib 32)`), it's the IO return time that counts for the ordering 

test2 = Stream.toList $ stream1 `parallel` stream2 `parallel` stream4 `parallel` stream3

dummyParallelExample :: IO [Int]
dummyParallelExample
  = Stream.fromList [40,40,40,40,40,40,40,40] --  @AsyncT 5 <> nToZero @AsyncT 6 <> nToZero @AsyncT 2
 |> Stream.mapM (fib .> evaluate)
 |> Stream.maxThreads numCapabilities  -- AheadT IO (Bool, Counts)
 |> Stream.fromParallel
 |> Stream.toList

test4 :: IO [Int]
test4
  -- = pure 30 `consM` pure 15 `consM` pure 5 `consM` Stream.nil
  = Stream.fromList [30, 15, 5] --  @AsyncT 5 <> nToZero @AsyncT 6 <> nToZero @AsyncT 2
 |> Stream.mapM (fib .> evaluate)
 |> Stream.maxThreads numCapabilities
 |> Stream.fromParallel
 |> Stream.toList

xs :: Serial Int
xs = Stream.fromList [3, 4] |> Stream.mapM delay

ys :: Serial Int
ys = Stream.fromList [1, 2]

-- slow :: Serial String
slow
  = Stream.repeatM (pure "tick")
 |> Stream.avgRate 1
 |> Stream.fromAhead
 |> Stream.mapM_ print



test5
  = Stream.fromList [30,29..0]
 |> Stream.fromSerial
 |> Stream.mapM ((+1) .> evaluate)
 |> Stream.map fib
 |> Stream.fromParallel
 |> Stream.mapM_ print

test6
  = Stream.fromList [4,3..0]
 |> Stream.fromSerial
 |> Stream.mapM (delay)
 |> Stream.fromSerial
 |> Stream.mapM_ print

printStream :: (Show a) => Serial a -> IO ()
printStream = Stream.mapM_ print

loops :: (IsStream t, MonadIO (t IO)) => t IO ()
loops = do
  x <- Stream.fromList [1,2]
  y <- Stream.fromList [3,4]
  print (x,y) |> liftIO

