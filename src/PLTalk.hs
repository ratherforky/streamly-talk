{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TypeApplications #-}
module PLTalk where

import Control.Concurrent (threadDelay)

import Flow

import qualified Streamly.Data.Array.Foreign as Array
import qualified Streamly.Data.Fold as Fold
import qualified Streamly.Data.Unfold as Unfold
import qualified Streamly.Prelude as Stream
import Data.Functor.Identity (Identity (runIdentity))
import Streamly.Prelude (IsStream (consM), ParallelT, AsyncT, SerialT, parallel, Serial)
import Streamly.Data.Fold (Fold)
import GHC.Conc (numCapabilities)
import Control.Exception (evaluate)
-- import Control.Monad ((>=>))
import Streamly.Data.Fold.Tee (Tee(Tee, toFold))
import Data.Functor (void)

import Control.DeepSeq
import System.IO.Unsafe


-- Streams https://hackage.haskell.org/package/streamly-0.8.0/docs/Streamly-Prelude.html#g:50

delay :: Int -> IO Int
delay n = do
  threadDelay (n * 1000000)   -- sleep for n seconds
  putStrLn (show n ++ " sec") -- print "n sec"
  return n                    -- IO Int

xs :: SerialT IO Int
-- xs = Stream.fromList [1,2,3]
-- xs = delay 1 `consM` delay 3 `consM` Stream.empty
xs = Stream.fromListM [delay 1, delay 3]

nums :: Serial Int
nums = Stream.fromList [1..30]

-- Folds https://hackage.haskell.org/package/streamly-0.8.0/docs/Streamly-Data-Fold.html

meanFold :: Fold IO Int Int
meanFold = Fold.teeWith div Fold.sum Fold.length

stats :: Fold IO Int (Int,Int,Int,Int)
stats 
  = toFold
  $ (,,,)
 <$> Tee Fold.sum
 <*> Tee Fold.length
 <*> Tee meanFold
 <*> Tee Fold.product

-- Composing folds https://hackage.haskell.org/package/streamly-0.8.0/docs/Streamly-Data-Fold-Tee.html


-- Unfolds https://hackage.haskell.org/package/streamly-0.8.0/docs/Streamly-Data-Unfold.html



-- Concurrency 

parStream :: ParallelT IO Int
-- parStream = Stream.fromList [1,2,3,4,5,6]
parStream = Stream.fromListM [delay 3, delay 1]

parStream' :: IsStream t => t IO Int
parStream' = Stream.fromList [1,2] `parallel` Stream.fromList [1,2]

printStream :: (Show a) => Serial a -> IO ()
printStream = Stream.mapM_ print

foo
  = parStream 
 |> Stream.map (+1)
 |> Stream.fromParallel
 |> printStream

fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib !n = fib (n-1) + fib (n-2)

dummyParallelExample :: IO [Int]
dummyParallelExample
  = Stream.fromList [31,30,29,28] --  @AsyncT 5 <> nToZero @AsyncT 6 <> nToZero @AsyncT 2
--  |> Stream.mapM (\n -> (fib n |> print) >> delay 1 ) -- Try deepseq instead
 |> Stream.map (fib .> force)
--  |> Stream.mapM (fib .> force .> pure)
--  |> Stream.mapM (fib .> force .> evaluate)
--  |> Stream.map (fib .> evaluate .> unsafePerformIO)
--  |> Stream.map (fib .> printReturn .> unsafePerformIO .> force)
 |> Stream.trace (const 1 .> print)
 |> Stream.maxThreads numCapabilities 
 |> Stream.take 2 -- Weird, not taking the right number
 |> Stream.fromParallel
 |> Stream.toList

printReturn :: Show a => a -> IO a
printReturn x = print x >> pure x

pureParallelTest :: [Int]
pureParallelTest
  = Stream.fromList [31,30,29,28]
--  |> Stream.mapM (\n -> (fib n |> print) >> delay 1 )
--  |> Stream.map (fib .> force)
--  |> Stream.mapM (fib .> force .> pure)
--  |> Stream.mapM (fib .> force .> evaluate)
--  |> Stream.map (fib .> evaluate .> unsafePerformIO)
 |> Stream.map (fib .> evaluate .> unsafePerformIO .> force)
--  |> Stream.mapM (fib .> evaluate .> unsafePerformIO .> force .> pure) -- mapM requires MonadAsync
--  |> Stream.trace (const 1 .> print)
--  |> Stream.map (fib .> printReturn .> unsafePerformIO .> force)
 |> Stream.tap unsafePrintFold
 |> Stream.maxThreads numCapabilities  
 |> Stream.fromParallel
 |> Stream.toList
 |> runIdentity

deepseqFold :: Fold Identity Int ()
deepseqFold = Fold.foldl' (\_ x -> x `deepseq` ()) ()

unsafePrintFold :: Fold Identity Int ()
-- unsafePrintFold = Fold.foldl' (\_ x -> x `deepseq` ()) ()
unsafePrintFold = Fold.foldlM' (\_ x -> print x |> unsafePerformIO |> pure) (pure ())

bar = dummyParallelExample |> void
