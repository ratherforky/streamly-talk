{-# language TypeApplications, ScopedTypeVariables #-}
module AnagramGenerator where

-- Check anagrams in parallel

import Streamly.Unicode.Stream
import qualified Streamly.Internal.Unicode.Array.Char as AChar
import qualified Streamly.Internal.Unicode.Stream as SChar
import qualified Streamly.Internal.FileSystem.FD as FD
import Streamly.Data.Array.Foreign (Array)
import qualified Streamly.Data.Array.Foreign as Arr
import qualified Streamly.Prelude as Stream
import qualified Streamly.Internal.Data.Fold.Type as Fold

import System.IO
import Control.Monad.IO.Class
import Flow
import Streamly.Prelude
import Data.Word
import Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet
import Data.Hashable (Hashable (hashWithSalt))
import Foreign (Storable)
import Data.Char (toLower)
import Data.Functor.Identity (Identity)
import Combinatorics (permuteFast)
import Data.Functor ((<&>))
import Control.Monad.Trans.Class ( MonadTrans(lift) )

-- Generate and test is factorial complexity, infeasible
-- Trie for dictionary + compute all paths using data would be hugely more efficient
anagramMain :: IO ()
anagramMain = do
  dict <- getDict
  anagrams dict "britney" |> printStream

readDictFile :: Serial Word8
readDictFile
   = liftIO (FD.openFile "dictionary.txt" ReadMode)
 >>= FD.read

instance (Hashable a, Storable a) => Hashable (Array a) where
  hashWithSalt salt arr = hashWithSalt salt (Arr.toList arr) -- Uses lists, not great

-- >>> HashSet.singleton (Arr.fromList ['a','b','c']) 
-- fromList ["abc"]

getDict :: IO (HashSet String)
getDict
  = readDictFile -- Serial Word8
 |> decodeUtf8   -- Serial Char
 |> fmap toLower -- Serial Char
 |> SChar.lines Fold.toList -- Serial String
 |> toHashSet    -- IO (HashSet String)

toHashSet :: (Eq a, Hashable a)
          => Serial a
          -> IO (HashSet a)
toHashSet = Stream.foldl' f k
  where
    f hashSet x = HashSet.insert x hashSet

    k = HashSet.empty

-- sprinkle :: a -> Serial a -> Serial (Serial a)
-- sprinkle x xs = undefined

-- No bounds check
-- If index exceeds length, inserts at end
-- If index negative, inserts at front
insertAt :: (IsStream t, Monad m) => a -> t m a -> Int -> t m a
insertAt x xs i
  = Stream.indexed xs
 |> Stream.insertBy cmpIndex (i, x)
 |> Stream.map snd
 where
   cmpIndex (li,_) (ri,_) = compare li ri

type SerialPure = SerialT Identity

testStream :: SerialPure Char
testStream = Stream.fromList ['a'..'d']

permutationsMono :: String -> Serial String
permutationsMono = permuteFast .> Stream.fromList

anagrams :: HashSet String -> String -> Serial String
anagrams dict
  = permutationsMono
 .> Stream.filter (`HashSet.member` dict)

printStream :: (Show a) => Serial a -> IO ()
printStream = Stream.mapM_ print 
-- permutations :: forall t m a . (IsStream t, Monad m, Monad (t m), MonadTrans t)
--              => SerialT m a -> t m (t m a)
-- permutations xs = do
--   perms <- lift (Stream.toList xs <&> permuteFast)
--   Stream.fromList $ fmap Stream.fromList perms

-- >>> permutations testStream |> 

-- permutations :: Serial a -> Serial (Serial a)
-- permutations xs0 = xs0 `cons` perms xs0 nil
--   where
--     perms nil     _  = nil
--     perms (t:ts) is = undefined -- foldr interleave (perms ts (t:is)) (permutations is)
--       where interleave    xs     r = let (_,zs) = interleave' id xs r in zs
--             interleave' _ []     r = (ts, r)
--             interleave' f (y:ys) r = let (us,zs) = interleave' (f . (y:)) ys r
--                                      in  (y:us, f (t:y:us) : zs)

-- More general

-- getDict :: (MonadIO mIO, IsStream t) => t m Char -> mIO (Array Char)
-- getDict
--   = SChar.lines
  
--   -- turn into hashset

-- toHashSet :: (Eq a, Hashable a, MonadIO mIO)
--           => SerialT mIO a
--           -> mIO (HashSet a)
-- toHashSet = Stream.foldl' f k
--   where
--     f hashSet x = HashSet.insert x hashSet

--     k = HashSet.empty
