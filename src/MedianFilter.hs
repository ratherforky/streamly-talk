{-# LANGUAGE BangPatterns #-}
{-# language TypeApplications, ScopedTypeVariables, FlexibleContexts #-}
module MedianFilter where

-- Check anagrams in parallel

import Streamly.Unicode.Stream
import qualified Streamly.Internal.Unicode.Array.Char as AChar
import qualified Streamly.Internal.Unicode.Stream as SChar
import qualified Streamly.Internal.FileSystem.FD as FD
import Streamly.Data.Array.Foreign (Array)
import qualified Streamly.Internal.Data.Array.Foreign as Arr
import qualified Streamly.Prelude as Stream
import qualified Streamly.Internal.Data.Fold as Fold
import qualified Streamly.Internal.Data.Stream.IsStream.Top as StreamTop
import Streamly.Data.Fold.Tee (Tee (toFold))

import System.IO
import Control.Monad.IO.Class
import Flow
import Streamly.Prelude
import Data.Word
import Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet
import Data.Hashable (Hashable (hashWithSalt))
import Foreign (Storable)
import Data.Char (toLower)
import Data.Functor.Identity (Identity, runIdentity)
import Combinatorics (permuteFast)
import Data.Functor ((<&>))
import Control.Monad.Trans.Class ( MonadTrans(lift) )
import Codec.Picture
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec
import Data.Maybe (fromJust)
import Data.Strict.Tuple ( Pair ((:!:)) )
import qualified Data.Strict.Tuple as STup
import Debug.Trace
import Data.Bifunctor (bimap)
import Streamly.Internal.Data.Fold (Fold)
import Streamly.Internal.Data.Fold.Tee (Tee(Tee))
import Control.Applicative
import Control.Exception (evaluate)
import GHC.Conc (numCapabilities)

type PSerial = SerialT Identity -- Pure serial stream


readShipType :: IO ()
readShipType = do
  shipPng <- readPng "ship.png"
  case shipPng of
    Left err -> putStrLn err
    Right img -> putStrLn $ getImgType img

readShip :: IO (Either String (Image PixelRGB8))
readShip = readRGB8Png "ship.png"

readRGB8Png :: FilePath -> IO (Either String (Image PixelRGB8))
readRGB8Png inPath = do
  shipPng <- readPng inPath
  case shipPng of
    Left err              -> pure $ Left err
    Right (ImageRGB8 img) -> pure $ Right img
    Right _               -> pure $ Left "Png was read successfully, but was not RGB 8bit"

unsafeReadShip :: IO (Image PixelRGB8)
unsafeReadShip = do
  Right img <- readShip
  pure img

-- Unsafe pointer conversion might be possible here, but I don't currently
-- understand these types enough to try this
vectorToArray :: Storable a => Vector a -> Array a
vectorToArray = Vec.toList .> Arr.fromList -- Prob not efficient, but still O(n)

--  |> Arr.fromStreamN (Vec.length xs)
  -- where
  --   (ptr, length) = Vec.unsafeToForeignPtr0 xs


data ImageArray a = ImageArr
    { -- | Width of the image in pixels

      imageArrWidth  :: {-# UNPACK #-} !Int
      -- | Height of the image in pixels.

    , imageArrHeight :: {-# UNPACK #-} !Int

      -- | Image pixel data. To extract pixels at a given position

      -- you should use the helper functions.

      --

      -- Internally pixel data is stored as consecutively packed

      -- lines from top to bottom, scanned from left to right

      -- within individual lines, from first to last color

      -- component within each pixel.

    , imageArrData   :: Array (PixelBaseComponent a)
    }


toImageArray
  :: Storable (PixelBaseComponent a)
  => Image a -> ImageArray a
toImageArray (Image    w h vec)
            = ImageArr w h (vectorToArray vec)

-- If Image less than 3x3, all goes to shit
chunk3x3 :: (Monad m, Pixel pixel, IsStream stream)
         => Image pixel
         -> stream m (PSerial pixel)
chunk3x3 img@(Image w h _)
  = kernelIndexes w h
 |> Stream.fromParallel
 |> Stream.map (Stream.map (STup.uncurry $ pixelAt img))

unsafeIndexImage :: Pixel a => Image a -> Int -> a
unsafeIndexImage (Image _ _ img) = unsafePixelAt img

-- kernelIndexes :: (Monad m, IsStream stream) => Int -> Int -> stream m (stream m (Pair Int Int))
kernelIndexes :: (IsStream t1, IsStream t2, Monad m1, Monad m2) =>
                 Int -> Int -> t1 m1 (t2 m2 (Pair Int Int))
kernelIndexes !w !h
  = Stream.enumerateFromTo 0 (h - 3) |> Stream.concatMap
    (\dy -> Stream.enumerateFromTo 0 (w - 3) |> Stream.map
    (\dx -> Stream.map (bimap (+ dx) (+ dy)) kernelBaseIndexes))

kernelIndexes1D :: Monad m => Int -> Int -> SerialT m (SerialT m Int)
kernelIndexes1D !w !h
  = increments w h
 |> Stream.map (\inc -> Stream.map (+inc) $ kernelBaseIndexes1D w)

kernelBaseIndexes :: (Monad m, IsStream stream) => stream m (Pair Int Int)
kernelBaseIndexes = Stream.fromList [ x :!: y | x <- [0,1,2], y <- [0,1,2]]

kernelBaseIndexes1D :: Monad m => Int -> SerialT m Int
kernelBaseIndexes1D !w = Stream.fromList [ row * w + col | row <- [0,1,2], col <- [0,1,2]]


increments :: Monad m => Int -> Int -> SerialT m Int
increments !w !h
  = Stream.enumerateFromThenTo 0 w (w * (h - 3)) -- h-3 because this is index of top left of kernel
 |> Stream.concatMap (\startIndex -> Stream.enumerateFromTo startIndex (startIndex + w - 3)) -- w-3 because this is index of top left of kernel


-- Incomplete, just takes middle pixel atm
-- medianKernel :: (Monad m, Ord a) => SerialT m a -> m a
medianKernel :: Ord a => PSerial a -> a
medianKernel xs
  = (StreamTop.sortBy compare xs
      |> Stream.fold (Fold.index 4)) <&> fromJust
    |> runIdentity -- unsafe

-- >>> averageKernel (Stream.fromList [PixelRGB8 10 1 1, PixelRGB8 2 11 2, PixelRGB8 3 3 12])
-- PixelRGB8 5 5 5

-- Seems to create very dark images, unsure why
averageKernel :: PSerial PixelRGB8 -> PixelRGB8
averageKernel = Stream.fold meanPixel .> runIdentity
  where
    meanPixel :: Fold Identity PixelRGB8 PixelRGB8
    meanPixel = toFold
              $ PixelRGB8
            <$> meanComponent getR
            <*> meanComponent getG
            <*> meanComponent getB

    meanComponent :: (PixelRGB8 -> Pixel8) -> Tee Identity PixelRGB8 Pixel8
    meanComponent getComp = Tee $ Fold.lmap getComp meanRounded -- Fold.mean 

    meanRounded :: (Monad m, Integral a) => Fold m a a
    meanRounded = liftA2 div (Tee Fold.sum)
                           (Tee Fold.length <&> fromIntegral)
               |> toFold

    getR (PixelRGB8 r _ _) = r
    getG (PixelRGB8 _ g _) = g
    getB (PixelRGB8 _ _ b) = b

testImage :: Int -> Int -> Image Pixel8
testImage w h = generateImage (\x y -> fromIntegral $ y * w + x) w h

testImageRGB :: Int -> Int -> Image PixelRGB8
testImageRGB w h = generateImage (\x y -> let p = fromIntegral $ y * w + x in PixelRGB8 p p p) w h


printImageVals :: (Show (PixelBaseComponent a), Storable (PixelBaseComponent a)) => Image a -> IO ()
printImageVals (Image w h img) = print img

runMedianFilter :: -- (Pixel a, Ord a, Show a)
                  --  ( Storable (PixelBaseComponent a)
                  --  , Ord (PixelBaseComponent a)
                  --  , Show (PixelBaseComponent a))
                  ()
                => Image PixelRGB8
                -> IO (Image PixelRGB8)
runMedianFilter
  = runKernel medianKernel

runKernel :: -- (Pixel a, Ord a, Show a)
                  --  ( Storable (PixelBaseComponent a)
                  --  , Ord (PixelBaseComponent a)
                  --  , Show (PixelBaseComponent a))
                  ()
                => (PSerial PixelRGB8 -> PixelRGB8)
                -> Image PixelRGB8
                -> IO (Image PixelRGB8)
runKernel kernel img@(Image w h _)
  = (img -- I don't think the Vector in Image can be accessed in parallel :/
 |> chunk3x3 @IO
--  |> Stream.maxThreads 4
--  |> traceShowId
--  |> Stream.mapM (kernel .> pure)
 |> Stream.mapM (kernel .> evaluate) 
--  |> Stream.map (kernel) 
 |> Stream.maxThreads numCapabilities 
 |> Stream.concatMap (\(PixelRGB8 r g b) -> Stream.fromList [r,g,b])
 |> Stream.fromAhead -- Switching this to Ahead makes it way slower
 |> Stream.toList)
 <&> (Vec.fromList .> Image (w-2) (h-2))
 
--  |> traceShowId


unsafeGetIndex :: Int -> SerialT Identity a -> a
unsafeGetIndex i xs
  = Stream.fold (Fold.index i) xs
 |> runIdentity
 |> fromJust

filterShip :: FilePath -> IO ()
filterShip = kernelShip medianKernel

kernelShip :: (PSerial PixelRGB8 -> PixelRGB8) -> FilePath -> IO ()
kernelShip kernel = kernelOnFile kernel "ship.png"

kernelOnFile :: (PSerial PixelRGB8 -> PixelRGB8) -> FilePath -> FilePath -> IO ()
kernelOnFile kernel inPath outPath = do
  shipOrErr <- readRGB8Png inPath
  case shipOrErr of
    Left err -> putStrLn err
    Right img -> do
      img' <- runKernel kernel img
      writePng outPath img'

kernelOnFileNoOP :: (PSerial PixelRGB8 -> PixelRGB8) -> FilePath -> FilePath -> IO ()
kernelOnFileNoOP kernel inPath outPath = do
  shipOrErr <- readRGB8Png inPath
  case shipOrErr of
    Left err -> putStrLn err
    Right img -> do
      writePng outPath (id img)

printStream :: (Show a) => Serial a -> IO ()
printStream = Stream.mapM_ print


getImgType :: DynamicImage -> String
getImgType (ImageY8 _)        = "ImageY8"
getImgType (ImageY16   _6)    = "ImageY16"
getImgType (ImageY32   _2)    = "ImageY32"
getImgType (ImageYF    _)     = "ImageYF"
getImgType (ImageYA8   _A8)   = "ImageYA8"
getImgType (ImageYA16  _A16)  = "ImageYA16"
getImgType (ImageRGB8  _GB8)  = "ImageRGB8"
getImgType (ImageRGB16 _GB16) = "ImageRGB16"
getImgType (ImageRGBF  _GBF)  = "ImageRGBF"
getImgType (ImageRGBA8 _GBA8) = "ImageRGBA8"
getImgType (ImageRGBA16 _16)  = "ImageRGBA16"
getImgType (ImageYCbCr8 _r8)  = "ImageYCbCr8"
getImgType (ImageCMYK8  _8)   = "ImageCMYK8"
getImgType (ImageCMYK16 _16)  = "ImageCMYK16"
