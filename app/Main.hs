{-# language TypeApplications #-}
module Main where

import Lib
import AnagramGenerator (anagramMain)
import MedianFilter
import Intro
import WordCountParallel
import Control.Monad (void)
import Flow

main :: IO ()
main = kernelOnFile medianKernel "ship.png" "out3.png"
-- main = anagramMain
-- main = getWords
-- main = sumFibs 40 >>= print
-- main = wcmain
-- main = test2 >>= print
-- main = dummyParallelExample |> void
